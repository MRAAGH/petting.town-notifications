# [petting.town](https://petting.town) desktop notifications for Linux!

![pettings](https://img.ourl.ca/pettings.png)

It checks once every minute if you got more pettings! Browser doesn't need to be open. By the way, [pet me!](https://petting.town/mazie-%E2%9D%A4)

# How to install

download `petting-notification.sh` and put it somewhere (for example in `~/bin/`)

# How to run

```~/bin/petting-notification.sh "https://petting.town/YOUR-PET-LINK"```

For example:

```~/bin/petting-notification.sh "https://petting.town/mazie-❤"```

You can also add an icon!

```~/bin/petting-notification.sh "https://petting.town/mazie-❤" ~/Pictures/mazie_icon.png```

# How to run automatically

```
killall petting-notification
~/bin/petting-notification.sh "https://petting.town/mazie-❤" ~/Pictures/mazie_icon.png &
```

Put these lines somewhere where they will be run automatically!

For example, in your desktop environment's startup file, or in `~/.profile`, or `~/.bash_profile`, or `~/.bash_login`.

Don't put it in `~/.bashrc`!
Also don't put it in `/etc/init.d/`!
And don't use `cron` because cron is annoying!

# Need this on Windows

no