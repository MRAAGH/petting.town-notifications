#!/bin/bash

# This script checks petting.town every minute
# and displays a notification if you got new pettings.

if [ $# -lt "1" ]; then
    echo Please give your pet link as an argument!
    exit
fi
while :; do
    new=$(wget -qO- "$1" | grep -A 1 received-total | tail -1 | tr -d ' ')
    old=$(cat ~/.cache/petting)
    if [ "$new" -gt "$old" ]; then
        echo "$new" >~/.cache/petting
        if [ $# -lt "2" ]; then
            notify-send "$new pettings!"
            exit
        else
            notify-send "$new pettings!" -i "$2"
        fi
    fi
    sleep 60
done
